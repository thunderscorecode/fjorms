import { Component, Host, h, Prop, State, Event, EventEmitter } from '@stencil/core';
import { FjormField } from '../../fjorm.interfaces'

@Component({
  tag: 'fjorm-form',
  styleUrl: 'fjorm-form.css',
  shadow: true,
})
export class FjormForm {

  @Prop() name: string = 'form';

  @Prop() fields: string;
  fieldsInternal: FjormField[];

  @Prop() data: string;
  @State() dataInternal: any;

  @Event({
    composed: true,
    bubbles: false,
  }) formDataChange: EventEmitter<any>;

  componentWillLoad() {
    // we should not modify the prop, lets make an internal copy of it to emit later
    this.fieldsInternal = JSON.parse(this.fields)
    this.dataInternal = JSON.parse(this.data)
  }

  getFieldValue = (field: FjormField, key: string) => {
    if (field && field[key]) {
      return field[key];
    } else {
      return null;
    }
  }
  getDataValue = (key: string, form: boolean = false) => {
    if (this.dataInternal && this.dataInternal[key]) {
      return this.dataInternal[key];
    } else {
      return form ? {} : ''
    }
  }
  setDataValue = (key: string, value: any) => {
    this.dataInternal[key] = value;
  }
  handleFieldChange = (evt) => {
    this.setDataValue(evt.detail.field, evt.detail.value)
    this.formDataChange.emit({ name: this.name, data: this.dataInternal });
  }
  handleFormChange = (evt) => {
    this.setDataValue(evt.detail.field, evt.detail.value)
    this.formDataChange.emit({ name: this.name, data: this.dataInternal });
  }

  render() {
    return (
      <Host>
        {this.fieldsInternal ? (
          <div>
            {this.fieldsInternal.map((field: any, index: number) => (
              <div key={index}>
                {field.type === 'form' ? (
                  <fjorm-form
                    fields={JSON.stringify(field.fields)}
                    data={JSON.stringify(this.getDataValue(field.field, true))}
                    onFormDataChange={this.handleFormChange}
                    name={this.getFieldValue(field, 'name')}
                  />
                ) : (
                  <fjorm-input
                    field={this.getFieldValue(field, 'field')}
                    type={this.getFieldValue(field, 'type')}
                    placeholder={this.getFieldValue(field, 'placeholder')}
                    value={this.getDataValue(field.field)}
                    label={this.getFieldValue(field, 'label')}
                    hint={this.getFieldValue(field, 'hint')}
                    onValueChange={this.handleFieldChange}
                  />
                )}
              </div>
            ))}
          </div>
        ) : (
          <div>No fields were passed into this Fjorm.</div>
        )
        }

      </Host>
    );
  }

}
