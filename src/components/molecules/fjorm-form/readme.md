# fjorm-form



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type           | Default     |
| -------- | --------- | ----------- | -------------- | ----------- |
| `data`   | `data`    |             | `any`          | `undefined` |
| `fields` | --        |             | `FjormField[]` | `undefined` |
| `name`   | `name`    |             | `string`       | `'form'`    |


## Events

| Event            | Description | Type               |
| ---------------- | ----------- | ------------------ |
| `formDataChange` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [fjorm-form](.)

### Depends on

- [fjorm-input-string](../fjorm-input-string)
- [fjorm-form](.)
- [fjorm-select](../fjorm-select)

### Graph
```mermaid
graph TD;
  fjorm-form --> fjorm-form
  fjorm-input-string --> fjorm-label
  fjorm-input-string --> fjorm-hint
  style fjorm-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
