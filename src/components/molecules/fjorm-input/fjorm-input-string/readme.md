# fjorm-input-string



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type     | Default     |
| ------------- | ------------- | ----------- | -------- | ----------- |
| `field`       | `field`       |             | `string` | `undefined` |
| `hint`        | `hint`        |             | `string` | `undefined` |
| `label`       | `label`       |             | `string` | `undefined` |
| `placeholder` | `placeholder` |             | `string` | `undefined` |
| `value`       | `value`       |             | `string` | `undefined` |


## Events

| Event         | Description | Type                           |
| ------------- | ----------- | ------------------------------ |
| `valueChange` |             | `CustomEvent<FjormFieldEvent>` |


## Dependencies

### Used by

 - [fjorm-form](../fjorm-form)

### Depends on

- [fjorm-label](../fjorm-label)
- [fjorm-hint](../fjorm-hint)

### Graph
```mermaid
graph TD;
  fjorm-input-string --> fjorm-label
  fjorm-input-string --> fjorm-hint
  fjorm-form --> fjorm-input-string
  style fjorm-input-string fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
