import { Component, Host, h, Prop, Event, EventEmitter, State } from '@stencil/core';
import { FjormFieldEvent } from '../../../fjorm.interfaces'

@Component({
  tag: 'fjorm-input-string',
  styleUrl: 'fjorm-input-string.css',
  shadow: true,
})
export class FjormInputString {
  @Prop({ attribute: 'required' }) required: boolean;
  @Prop() value: string;
  @Prop() field: string;
  @Prop() label: string;
  @Prop() hint: string;
  @Prop() placeholder: string;
  @Prop() props: any;

  @Event() valueChange: EventEmitter<FjormFieldEvent>;

  @State() valueInner: string;
  handleChange(event) {
    this.valueChange.emit({ field: this.field, value: event.target.value });
  }

  render() {
    return (
      <Host>
        {this.field ? (
            <input
              type="text"
              value={this.value}
              placeholder={this.placeholder}
              onInput={(event) => this.handleChange(event)}
            />
        ) : (
          <div style={{ color: 'red' }}>This input was not passed a field name.</div>
        )}

      </Host>
    );
  }

}
