import { Component, Host, h, Prop, Event, EventEmitter, State } from '@stencil/core';

@Component({
  tag: 'fjorm-input-number',
  styleUrl: 'fjorm-input-number.css',
  shadow: true,
})
export class FjormInputNumber {
  @Prop() value: string;
  @Event() valueChange: EventEmitter<string>;

  @State() valueInner: string;
  handleChange(event) {
    this.valueChange.emit(event.target.value);
  }

  render() {
    return (
      <Host>
        <input type="text" value={this.value} onInput={(event) => this.handleChange(event)} />
      </Host>
    );
  }

}
