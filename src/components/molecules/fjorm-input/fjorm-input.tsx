import { Component, Host, h, Prop, Event, EventEmitter, State } from '@stencil/core';
import { FjormFieldEvent } from '../../fjorm.interfaces'

@Component({
  tag: 'fjorm-input',
  styleUrl: 'fjorm-input.css',
  shadow: true,
})
export class FjormInput {
  @Prop({ attribute: 'required' }) required: boolean;
  @Prop() type: string;
  @Prop() value: string;
  @Prop() field: string;
  @Prop() label: string;
  @Prop() hint: string;
  @Prop() options: string;
  @Prop() placeholder: string;

  @Event() valueChange: EventEmitter<FjormFieldEvent>;

  @State() valueInner: string;
  handleChange(event) {
    this.valueChange.emit({ field: this.field, value: event.target.value });
  }

  render() {

    const props = {
      required: this.required,
      type: this.type,
      value: this.value,
      options: this.options,
      field: this.field,
      label: this.label,
      hint: this.hint,
      placeholder: this.placeholder
    }

    return (
      <Host>
        {props.field ? (
          <div class="form-container">

            {props.label && (
              <div>
                <fjorm-label required={props.required} label={props.label} />
              </div>
            )}
            {props.hint && (
              <fjorm-hint hint={props.hint} />
            )}

            {
              {
                'text': <fjorm-input-string {...props} onValueChange={this.handleChange} />,
                'number': <fjorm-input-number {...props} onValueChange={this.handleChange} />,
                'color': <fjorm-input-color {...props} onValueChange={this.handleChange} />,
                'select': <fjorm-input-select {...props} onValueChange={this.handleChange} />,
              }[props.type]
            }

          </div>
        ) : (
          <div style={{ color: 'red' }}>This input was not passed a field name.</div>
        )}

      </Host>
    );
  }

}
