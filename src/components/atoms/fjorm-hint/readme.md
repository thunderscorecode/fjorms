# fjorm-hint



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `hint`   | `hint`    |             | `string` | `undefined` |


## Dependencies

### Used by

 - [fjorm-input-string](../fjorm-input-string)

### Graph
```mermaid
graph TD;
  fjorm-input-string --> fjorm-hint
  style fjorm-hint fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
