import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'fjorm-hint',
  styleUrl: 'fjorm-hint.css',
  shadow: true,
})
export class FjormHint {

  @Prop() hint: string;

  render() {
    return (
      <Host>
        <div class="hint">{this.hint}</div>
      </Host>
    );
  }

}
