import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'fjorm-label',
  styleUrl: 'fjorm-label.css',
  shadow: true,
})
export class FjormLabel {
  @Prop() label: string;
  @Prop() required: boolean;

  render() {
    return (
      <Host>
        <label class="label">
          {this.label}
          {!this.required && (
            <span class="label__optional"> (Optional)</span>
          )}</label>
      </Host>
    );
  }

}
