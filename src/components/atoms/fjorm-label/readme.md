# fjorm-label



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `label`  | `label`   |             | `string` | `undefined` |


## Dependencies

### Used by

 - [fjorm-input-string](../fjorm-input-string)

### Graph
```mermaid
graph TD;
  fjorm-input-string --> fjorm-label
  style fjorm-label fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
