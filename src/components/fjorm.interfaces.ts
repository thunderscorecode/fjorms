interface FjormField {
  name: string;
  value: string;
  placeholder: string;
}

interface FjormFieldEvent {
  field: string;
  value: number;
}

interface FjormOption {
  value: string;
  text: string;
}

export { FjormField, FjormFieldEvent, FjormOption }