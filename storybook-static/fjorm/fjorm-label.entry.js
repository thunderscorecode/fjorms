import { r as registerInstance, h, f as Host } from './index-a5a629f9.js';

const fjormLabelCss = ":host{display:block}.label{font-size:var(--fjorm-label-font-size);font-family:var(--fjorm-label-font-family);font-weight:var(--fjorm-label-font-weight);padding:var(--fjorm-label-padding);margin:var(--fjorm-label-margin);color:var(--fjorm-label-color)}";

const FjormLabel = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h(Host, null, h("label", { class: "label" }, this.label)));
  }
};
FjormLabel.style = fjormLabelCss;

export { FjormLabel as fjorm_label };
