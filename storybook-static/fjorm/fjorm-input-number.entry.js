import { r as registerInstance, e as createEvent, h, f as Host } from './index-a5a629f9.js';

const fjormInputNumberCss = ":host{display:block}";

const FjormInputNumber = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.valueChange = createEvent(this, "valueChange", 7);
  }
  handleChange(event) {
    this.valueChange.emit(event.target.value);
  }
  render() {
    return (h(Host, null, h("input", { type: "text", value: this.value, onInput: (event) => this.handleChange(event) })));
  }
};
FjormInputNumber.style = fjormInputNumberCss;

export { FjormInputNumber as fjorm_input_number };
