import { r as registerInstance, e as createEvent, h, f as Host } from './index-a5a629f9.js';

const fjormInputStringCss = ":host{display:block}.form-container{margin:var(--fjorm-input-container-margin);box-sizing:border-box}input[type=text]{box-sizing:border-box;width:100%;font-size:var(--fjorm-input-font-size);font-family:var(--fjorm-input-font-family);padding:var(--fjorm-input-padding);margin:var(--fjorm-input-margin);color:var(--fjorm-input-color);border:var(--fjorm-input-border);border-radius:var(--fjorm-input-border-radius);background:var(--fjorm-input-background)}input[type=text]::-webkit-input-placeholder{color:var(--fjorm-input-placeholder-color)}";

const FjormInputString = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.valueChange = createEvent(this, "valueChange", 3);
  }
  handleChange(event) {
    this.valueChange.emit({ field: this.field, value: event.target.value });
  }
  render() {
    return (h(Host, null, this.field ? (h("div", { class: "form-container" }, this.label && (h("fjorm-label", { label: this.label })), this.hint && (h("fjorm-hint", { hint: this.hint })), h("input", { type: "text", value: this.value, placeholder: this.placeholder, onInput: (event) => this.handleChange(event) }))) : (h("div", { style: { color: 'red' } }, "This input was not passed a field name."))));
  }
};
FjormInputString.style = fjormInputStringCss;

export { FjormInputString as fjorm_input_string };
