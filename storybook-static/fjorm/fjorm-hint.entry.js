import { r as registerInstance, h, f as Host } from './index-a5a629f9.js';

const fjormHintCss = ":host{display:block}.hint{font-size:var(--fjorm-hint-font-size);font-family:var(--fjorm-hint-font-family);font-weight:var(--fjorm-hint-font-weight);padding:var(--fjorm-hint-padding);margin:var(--fjorm-hint-margin);color:var(--fjorm-hint-color)}";

const FjormHint = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
  }
  render() {
    return (h(Host, null, h("div", { class: "hint" }, this.hint)));
  }
};
FjormHint.style = fjormHintCss;

export { FjormHint as fjorm_hint };
