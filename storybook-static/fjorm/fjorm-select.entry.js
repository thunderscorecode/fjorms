import { r as registerInstance, e as createEvent, h, f as Host } from './index-a5a629f9.js';

const fjormSelectCss = ":host{display:block}";

const FjormSelect = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.valueChange = createEvent(this, "valueChange", 3);
  }
  handleChange(event) {
    this.valueChange.emit({ field: this.field, value: event.target.value });
  }
  render() {
    return (h(Host, null, h("select", null, this.options.map((item, index) => (h("option", { key: index, value: item.value }, item.text))))));
  }
};
FjormSelect.style = fjormSelectCss;

export { FjormSelect as fjorm_select };
