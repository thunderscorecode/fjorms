import { r as registerInstance, e as createEvent, h, f as Host } from './index-a5a629f9.js';

const fjormFormCss = ":host{display:block}";

const FjormForm = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.formDataChange = createEvent(this, "formDataChange", 3);
    this.name = 'form';
    this.getFieldValue = (field, key) => {
      if (field && field[key]) {
        return field[key];
      }
      else {
        return null;
      }
    };
    this.getDataValue = (key, form = false) => {
      if (this.dataInternal && this.dataInternal[key]) {
        return this.dataInternal[key];
      }
      else {
        return form ? {} : '';
      }
    };
    this.setDataValue = (key, value) => {
      this.dataInternal[key] = value;
    };
    this.handleFieldChange = (evt) => {
      this.setDataValue(evt.detail.field, evt.detail.value);
      this.formDataChange.emit({ name: this.name, data: this.dataInternal });
    };
    this.handleFormChange = (evt) => {
      this.setDataValue(evt.detail.field, evt.detail.value);
      this.formDataChange.emit({ name: this.name, data: this.dataInternal });
    };
  }
  componentWillLoad() {
    this.dataInternal = this.data; // we should not modify the prop, lets make an internal copy of it to emit later
  }
  render() {
    return (h(Host, null, this.fields ? (h("div", null, this.fields.map((field, index) => (h("div", { key: index }, field.type === 'text' && (h("div", null, h("fjorm-input-string", { field: field.field, placeholder: this.getFieldValue(field, 'placeholder'), value: this.getDataValue(field.field), label: this.getFieldValue(field, 'label'), hint: this.getFieldValue(field, 'hint'), onValueChange: this.handleFieldChange }))), field.type === 'form' && (h("div", null, h("fjorm-form", { fields: field.fields, data: this.getDataValue(field.field, true), onFormDataChange: this.handleFormChange, name: this.getFieldValue(field, 'name') }))), field.type === 'select' && (h("div", null, h("fjorm-select", { field: field.field, placeholder: this.getFieldValue(field, 'placeholder'), value: this.getDataValue(field.field), label: this.getFieldValue(field, 'label'), hint: this.getFieldValue(field, 'hint'), options: this.getFieldValue(field, 'options'), onValueChange: this.handleFieldChange })))))))) : (h("div", null, "No fields were passed into this Fjorm."))));
  }
};
FjormForm.style = fjormFormCss;

export { FjormForm as fjorm_form };
