import { EventEmitter } from '../../stencil-public-runtime';
import { FjormField } from '../fjorm.interfaces';
export declare class FjormForm {
  name: string;
  fields: FjormField[];
  data: any;
  dataInternal: any;
  formDataChange: EventEmitter<any>;
  componentWillLoad(): void;
  getFieldValue: (field: FjormField, key: string) => any;
  getDataValue: (key: string, form?: boolean) => any;
  setDataValue: (key: string, value: any) => void;
  handleFieldChange: (evt: any) => void;
  handleFormChange: (evt: any) => void;
  render(): any;
}
