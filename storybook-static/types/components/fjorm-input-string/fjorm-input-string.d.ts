import { EventEmitter } from '../../stencil-public-runtime';
import { FjormFieldEvent } from '../fjorm.interfaces';
export declare class FjormInputString {
  value: string;
  field: string;
  label: string;
  hint: string;
  placeholder: string;
  valueChange: EventEmitter<FjormFieldEvent>;
  valueInner: string;
  handleChange(event: any): void;
  render(): any;
}
