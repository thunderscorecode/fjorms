import { EventEmitter } from '../../stencil-public-runtime';
export declare class FjormInputNumber {
  value: string;
  valueChange: EventEmitter<string>;
  valueInner: string;
  handleChange(event: any): void;
  render(): any;
}
