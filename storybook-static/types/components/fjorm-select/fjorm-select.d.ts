import { EventEmitter } from '../../stencil-public-runtime';
import { FjormFieldEvent, FjormOption } from '../fjorm.interfaces';
export declare class FjormSelect {
  value: string;
  field: any;
  label: string;
  hint: string;
  options: FjormOption[];
  placeholder: string;
  valueChange: EventEmitter<FjormFieldEvent>;
  valueInner: string;
  handleChange(event: any): void;
  render(): any;
}
