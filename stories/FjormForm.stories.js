import { html } from 'lit-html';

export default {
  title: 'Fjorm/Form',
  argTypes: {
    name: {
      control: 'text',
      description: 'When there are multiple forms on your page, you are going to need this.',
      defaultValue: 'myForm'
    },
    fields: {
      control: 'object',
      description: 'A JSON string defining the fields in this form.',
      defaultValue: [
        {
          field: 'firstName',
          label: 'First Name',
          hint: 'Type your first (given name) here.',
          type: 'text',
          placeholder: 'Your first name, e.g Timmy'
        },
        {
          field: 'lastName',
          label: 'Last Name',
          hint: 'Type your last (family name) here.',
          type: 'text',
          placeholder: 'Your last name, e.g McTimmerson'
        },
        {
          field: 'hasCake',
          label: 'Presence of Cake',
          hint: 'Select in this list, any cake you may have.',
          type: 'select',
          options: [
            { value: 'nocake', text: 'No Cake (booo)' },
            { value: 'cheesecake', text: 'Cheesecake' },
            { value: 'bfg', text: 'Black Forest Gateau' },
            { value: 'fruitcake', text: 'Fruitcake' },
          ]
        },
        {
          field: 'age',
          label: 'Age',
          hint: 'Without lying, type your age here.',
          type: 'text',
          placeholder: 'Age'
        },
        {
          field: 'address', type: 'form',
          fields: [
            { field: 'street', hint: 'Full street address, including house number.', label: 'Street Name', type: 'text', placeholder: 'Street' },
            { field: 'postcode', hint: 'Postal code, that we will validate for you.', label: 'Postcode', type: 'text', placeholder: 'Postcode' },
          ]
        }
      ],
    },
    data: {
      control: 'object',
      description: 'A JSON string defining the data to fill the form with.',
      defaultValue: {
        firstName: 'Lee',
        lastName: 'Nattress',
        age: '39',
        address: {
          street: '1 Test Street',
          postcode: 'T31 T5T'
        }
      },
    },
    formDataChange: {
      action: 'Form data changed'
    }
  },
};

export const Basic = ({ name, fields, data, formDataChange }) => {
  const handleChange = evt => formDataChange('formDataChange', evt.detail)
  return html`<fjorm-form @formDataChange="${handleChange}" name="${name}" fields="${JSON.stringify(fields)}" data="${JSON.stringify(data)}"></fjorm-form>`;
}

