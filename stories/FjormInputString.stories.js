import { html } from 'lit-html';

export default {
  title: 'Fjorm/String Input',
  argTypes: {
    field: {
      control: 'text',
      description: 'This will be its property name when returned as aprt of a fjorm-form.',
      defaultValue: 'myField',
    },
    label: {
      control: 'text',
      description: 'The label appears above the input and refers to the context of the input.',
      defaultValue: 'label text',
    },
    hint: {
      control: 'text',
      description: 'The hint appears between the label and the input and gives a little help as to what is expected of the user.',
      defaultValue: 'hint text',
    },
    placeholder: {
      control: 'text',
      description: 'This text appears inside the input when it is empty. It might contain an example input.',
      defaultValue: 'placeholder text',
    },
    valueChange: {
      action: 'Value changed'
    }
  },
};

export const Basic = ({ valueChange, field, label, hint, placeholder }) => {
  const handleChange = evt => valueChange('valueChange', evt.detail)
  return html`
  <fjorm-input @valueChange="${handleChange}" type="text" field="${field}" label="${label}" hint="${hint}" placeholder="${placeholder}"></fjorm-input>
  `;
}

export const Required = ({ valueChange, field, label, hint, placeholder }) => {
  const handleChange = evt => valueChange('valueChange', evt.detail)
  return html`
  <fjorm-input @valueChange="${handleChange}" type="text" required field="${field}" label="${label}" hint="${hint}" placeholder="${placeholder}"></fjorm-input>
  `;
}

export const InputOnly = ({ valueChange, field, label, hint, placeholder }) => {
  const handleChange = evt => valueChange('valueChange', evt.detail)
  return html`
  <fjorm-input-string @valueChange="${handleChange}" field="${field}" label="${label}" hint="${hint}" placeholder="${placeholder}"></fjorm-input-string>
  `;
}
